package com.tas.qlearning.tools;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class ObjectIdGenerator {

    private ObjectIdGenerator() throws InstantiationException {
        throw new InstantiationException();
    }

    static final String VARIABLE_SEPARATOR = " @@ ";

    public static String generateId(Object obj) throws IllegalAccessException, InvocationTargetException {
        if(obj instanceof AmbiguousIdObject){
            return getManualDefinedId((AmbiguousIdObject) obj);
        }else{
            return generateIdFromRawObject(obj);
        }
    }

    private static String generateIdFromRawObject(Object obj) throws IllegalAccessException, InvocationTargetException {
        StringBuilder generatedId = new StringBuilder();
        Field[] fields = obj.getClass().getDeclaredFields();
        Object[] values = getFieldValues(fields,obj);

        for (int i = 0; i < values.length; i++) {
            generatedId.append(getStringValue(values[i]));

            generatedId.append(VARIABLE_SEPARATOR);
        }
        return new String(generatedId);
    }


    private static Object[] getFieldValues(Field[] fields, Object instance) throws IllegalAccessException {
        Object[] values = new Object[fields.length];
        for (int i = 0; i <fields.length ; i++) {
            Field field = fields[i];
            field.setAccessible(true);
            values[i] = field.get(instance);
        }
        return values;
    }

    private static String getStringValue(Object value) throws InvocationTargetException, IllegalAccessException {
        if(value != null){
            try {
                Method m = value.getClass().getMethod(AmbiguousIdObject.METHOD_NAME);
                m.setAccessible(true);
                return  (String) m.invoke(value);
            } catch (NoSuchMethodException e) {
                return value.toString();
            }
        }else{
           return null+"";
        }
    }

    private static String getManualDefinedId(AmbiguousIdObject obj){
        return obj.idHelpGenerator();
    }

}

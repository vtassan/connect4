package com.tas.qlearning.tools;


import java.lang.reflect.InvocationTargetException;

import java.util.ArrayList;
import java.util.List;

import static com.tas.qlearning.tools.ObjectIdGenerator.generateId;

public class TestGenerator {

    public static void main(String... args) throws IllegalAccessException, InvocationTargetException {

        System.out.println(AmbiguousIdObject.METHOD_NAME);

        List<Integer> el = new ArrayList<>();
        el.add(2);
        el.add(3);

        TestClass tc = new TestClass(5,"Bonjour",el,new String[] {"case0","case1"});

        System.out.println(generateId(tc));

        TestClass tc2 = new TestClass(2, null,null,new String[]{});

        System.out.println(generateId(tc2));

        MetaTestClass mtc = new MetaTestClass(tc);

        System.out.println(generateId(mtc));
    }

}

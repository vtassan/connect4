package com.tas.qlearning.tools;

public class MetaTestClass implements AmbiguousIdObject{

    TestClass ct;

    public MetaTestClass(TestClass ct){
        this.ct = ct;
    }

    @Override
    public String idHelpGenerator() {
        return "Metaclass";
    }
}

package com.tas.qlearning;

import com.tas.qlearning.tools.ObjectIdGenerator;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.InvocationTargetException;

@Setter
@Getter
public abstract class State {

    protected double qValue;

    protected State(double qValue){
        this.qValue = qValue;
    }

    public final String getId() throws InvocationTargetException, IllegalAccessException {
        return ObjectIdGenerator.generateId(this);
    }

}

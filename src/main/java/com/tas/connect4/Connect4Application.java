package com.tas.connect4;

import com.tas.entity.Board;
import com.tas.entity.Connect4World;
import com.tas.entity.PlayerRandom;
import com.tas.tools.LoggerInterface;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

@SpringBootApplication
public class Connect4Application implements CommandLineRunner, LoggerInterface {

	public static final Logger LOGGER = Logger.getLogger(Connect4Application.class.toString());

	public static void main(String[] args) throws IOException {

		SpringApplication.run(Connect4Application.class,args);

	}

	@Override
	public void run(String... args) throws IOException {
		LOGGER.addHandler(new FileHandler("src/test/log/testClass.txt"));

		Board b = new Board();

		Connect4World game = new Connect4World(b, new PlayerRandom(1), new PlayerRandom(2));
		game.playGame();
		LOGGER.info("over");
//		game.writeXMLObject("src/test/xml/object.xml");
	}
}

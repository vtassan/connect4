package com.tas.entity;

import com.tas.tools.MatrixOperation;


import static com.tas.tools.RandomGenerator.getRandomColumn;


public class PlayerRandom extends Player{

    public PlayerRandom(int id){
        super(id);
    }

    @Override
    public Token playMove(Token[][] matrix) {
        int choice = getRandomColumn();
        while(MatrixOperation.isColumnFull(matrix,choice)){
            choice = getRandomColumn();
        }
        int ordinate = MatrixOperation.getFirstFreeSpot(matrix,choice);

        return new Token(choice,ordinate,this.id);
    }


}

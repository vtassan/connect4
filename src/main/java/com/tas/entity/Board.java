package com.tas.entity;

import com.tas.exception.IdNumberException;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import static com.tas.connect4.Connect4Application.LOGGER;

@XmlRootElement(name = "board")
// If you want you can define the order in which the fields are written
// Optional
@XmlType(propOrder = { "width", "height", "name", "matrix" })

public class Board {

    public static final int BOARD_WIDTH = 7;
    public static final int BOARD_HEIGHT = 6;


    private int width;
    private int height;
    private String name;
    private Token[][] matrix;

    /**
     * create an empty board
     */
    public Board() {
        this.width = BOARD_WIDTH;
        this.height = BOARD_HEIGHT;
        this.name = "Undefined Connect4 Game";
        this.initMatrix();
    }

    public Board(String name){
        this.width = BOARD_WIDTH;
        this.height = BOARD_HEIGHT;
        this.name = name;
        this.initMatrix();
    }

    private void initMatrix(){
        matrix = new Token[width][height];
    }


    @Override
    public String toString() {
        return "Board{" +
                "width=" + width +
                ", height=" + height +
                ", name='" + name + '\'' +
                '}';
    }

    public String drawMatrix(){
        StringBuilder draw = new StringBuilder();
        for(int j=matrix[0].length-1; j>=0; j--){
            draw.append('|');
            draw.append(' ');
            for(int i=0; i<matrix.length; i++){

                if (matrix[i][j] == null) {
                    draw.append(drawChar(0));
                }else{
                    draw.append(drawChar(matrix[i][j].getIdPlayer()));
                }
                draw.append(' ');
            }
            draw.append('|');
            draw.append('\n');
        }
        //draw.append("| _ _ _ _ _ _ _ |");
        return new String(draw);
    }

    private char drawChar(int idColor){
        switch(idColor){
            case 0:
                return '-';
            case 1:
                return 'o';
            case 2:
                return 'x';
            default:
                try {
                    throw new IdNumberException("id_color should only value 0, 1 or 2. Here, id_color = "+idColor);
                } catch (Exception e) {
                   LOGGER.warning(e.getMessage());
                }
                return ' ';
        }
    }

    public void placeToken(Token token){
        int abs = token.posX;
        int ord = token.posY;
        matrix[abs][ord] = token;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @XmlElementWrapper(name="tokens")
    @XmlElement(name="column")
    public Token[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(Token[][] matrix) {
        this.matrix = matrix;
    }


}

package com.tas.entity;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Token {

    int idPlayer;
    int posX;
    int posY;

    public Token(int posX, int posY, int idPlayer){
        this.posX = posX;
        this.posY = posY;
        this.idPlayer = idPlayer;
    }

    public Token(){}

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getIdPlayer() {
        return idPlayer;
    }

    public void setIdPlayer(int idPlayer) {
        this.idPlayer = idPlayer;
    }

    @Override
    public String toString() {
        return "Token{" +
                "id_color=" + idPlayer +
                ", posX=" + posX +
                ", posY=" + posY +
                '}';
    }
}

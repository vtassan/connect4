package com.tas.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "player")
// If you want you can define the order in which the fields are written
// Optional

public abstract class Player{

    @XmlElement
    protected int id;

    public Player(int id){
        this.id = id;
    }

    public Player() {}

    public abstract Token playMove(Token[][] matrix);

    public int getId() {
        return id;
    }
}

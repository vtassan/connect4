package com.tas.entity;

import com.tas.tools.CheckWin;

import javax.persistence.Entity;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static com.tas.connect4.Connect4Application.LOGGER;

@Entity
@XmlRootElement(namespace = "com.tas.entity")
@XmlType(propOrder = { "over", "players", "boardGame" })
public class Connect4World {

    private boolean over;
    // XmLElementWrapper generates a wrapper element around XML representation
    @XmlElementWrapper(name = "players")
    // XmlElement sets the name of the entities
    @XmlElement(name = "player")
    private Player[] players;
    @XmlElement(name = "boardgame")
    private Board boardGame;

    public Connect4World(Board boardGame, Player... players){
        this.players = players;
        this.boardGame = boardGame;
    }

    public Connect4World(){}


    public void playGame(){
        int activePlayer = 0;

        Token lastToken;
        while(!over){

            lastToken = players[activePlayer].playMove(boardGame.getMatrix());
            boardGame.placeToken(lastToken);
            String matrixDrawing = boardGame.drawMatrix();
            LOGGER.info("\n"+matrixDrawing);
            if(lastToken == null || CheckWin.checkWin(boardGame.getMatrix(),lastToken)) {
                this.over = true;
                LOGGER.info("Player "+players[activePlayer].getId()+" wins!!");
            } else
                activePlayer = (activePlayer+1) % players.length;


        }
    }



    public boolean isOver() {
        return over;
    }

    public void setOver(boolean over) {
        this.over = over;
    }


    public static void main(String... args) throws IOException, IllegalAccessException, JAXBException {
        //test XMLConverter
        Connect4World c4w = new Connect4World(new Board(),new PlayerRandom(0), new PlayerRandom(1));
        c4w.playGame();
        //c4w.writeXMLObject("src/test/xml/object.xml");
        c4w.writeJAXB("src/test/xml/object.xml");
    }

    private void writeJAXB(String s) throws JAXBException, FileNotFoundException {
        // create JAXB context and instantiate marshaller
        JAXBContext context = JAXBContext.newInstance(Connect4World.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to System.out
        m.marshal(this, System.out);

        // Write to File
        m.marshal(this, new File(s));

        // get variables from our xml file, created before
        System.out.println();
        System.out.println("Output from our XML File: ");
        Unmarshaller um = context.createUnmarshaller();
//        Bookstore bookstore2 = (Bookstore) um.unmarshal(new FileReader(s));
//        ArrayList<Book> list = bookstore2.getBooksList();
//        for (Book book : list) {
//            System.out.println("Book: " + book.getName() + " from "
//                    + book.getAuthor());
//        }

    }

}

package com.tas.exception;

public class IdNumberException extends Exception{

    public IdNumberException(String err){
        super(err);
    }
}

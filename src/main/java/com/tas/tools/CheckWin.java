package com.tas.tools;

import com.tas.entity.Token;

public final class CheckWin {

    public static boolean checkWin(Token[][] matrix, Token lastToken){
        return checkHorizontalWin(matrix,lastToken) || checkVerticalWin(matrix,lastToken) || checkRisingDiagonalWin(matrix,lastToken) || checkRecedingDiagonalWin(matrix,lastToken);

    }

    /**
     * checking if the last token played create a connect-4 for the receding diagonal
     * @param matrix matrix of the connect-4 game
     * @param lastToken last token played
     * @return
     */
    private static boolean checkRecedingDiagonalWin(Token[][] matrix, Token lastToken) {
        int alignedToken = 1;
        int abs = lastToken.getPosX();
        int ord = lastToken.getPosY();
        int i = 1;
        //check left
        //there are 4 conditions to test: the two first are testing the out of bounds, the third one is checking if there is a token and the last one is checking if both tokens have the same color/player
        while(abs-i >= 0 && ord-i >= 0 && matrix[abs-i][ord-i] != null && matrix[abs-i][ord-i].getIdPlayer() == lastToken.getIdPlayer()){
            alignedToken++;
            i++;
        }
        //check right
        //there are 4 conditions to test: the two first are testing the out of bounds, the third one is checking if there is a token and the last one is checking if both tokens have the same color/player
        i = 1;
        while(abs+i < matrix.length && ord+i < matrix[abs+i].length && matrix[abs+i][ord+i] != null && matrix[abs+i][ord+i].getIdPlayer() == lastToken.getIdPlayer()){
            alignedToken++;
            i++;
        }
        return alignedToken >= 4;
    }

    /**
     * checking if the last token played create a connect-4 for the rising diagonal
     * @param matrix matrix of the connect-4 game
     * @param lastToken last token played
     * @return
     */
    private static boolean checkRisingDiagonalWin(Token[][] matrix, Token lastToken) {
        int alignedToken = 1;
        int abs = lastToken.getPosX();
        int ord = lastToken.getPosY();
        int i = 1;

        //check left
        //there are 4 conditions to test: the two first are testing the out of bounds, the third one is checking if there is a token and the last one is checking if both tokens have the same color/player
        i = 1;
        while(abs-i >= 0 && ord+i < matrix[abs-i].length && matrix[abs-i][ord+i] != null && matrix[abs-i][ord+i].getIdPlayer() == lastToken.getIdPlayer()){
            alignedToken++;
            i++;
        }

        //check right
        //there are 4 conditions to test: the two first are testing the out of bounds, the third one is checking if there is a token and the last one is checking if both tokens have the same color/player
        while(abs+i < matrix.length && ord-i >= 0 && matrix[abs+i][ord-i] != null && matrix[abs+i][ord-i].getIdPlayer() == lastToken.getIdPlayer()){
            alignedToken++;
            i++;
        }
        return alignedToken >= 4;
    }

    /**
     * checking if the last token played create a connect-4 for the vertical line
     * @param matrix matrix of the connect-4 game
     * @param lastToken last token played
     * @return
     */
    private static boolean checkVerticalWin(Token[][] matrix, Token lastToken) {
        int alignedToken = 1;

        int i = 1;
        int abs = lastToken.getPosX();
        int ord = lastToken.getPosY();

        //check below
        //there are 3 conditions to test: the first one is testing the out of bounds, the second one is checking if there is a token and the last one is checking if both tokens have the same color/player
        while(ord-i >= 0 && matrix[abs][ord-i] != null && matrix[abs][ord-i].getIdPlayer() == lastToken.getIdPlayer()){
            alignedToken++;
            i++;
        }

        //check above
        //useless since no token can be above the last one

        return alignedToken >= 4;
    }

    /**
     * checking if the last token played create a connect-4 for the horizontal line
     * @param matrix matrix of the connect-4 game
     * @param lastToken last token played
     * @return
     */
    private static boolean checkHorizontalWin(Token[][] matrix, Token lastToken) {
        int alignedToken = 1;

        int i = 1;
        int abs = lastToken.getPosX();
        int ord = lastToken.getPosY();
        //check left
        //there are 3 conditions to test: the first one is testing the out of bounds, the second one is checking if there is a token and the last one is checking if both tokens have the same color/player
        while(abs-i >= 0 && matrix[abs-i][ord] != null && matrix[abs-i][ord].getIdPlayer() == lastToken.getIdPlayer()){
            alignedToken++;
            i++;
        }
        //check right
        //there are 3 conditions to test: the first one is testing the out of bounds, the second one is checking if there is a token and the last one is checking if both tokens have the same color/player
        i = 1;
        while(abs+i < matrix.length && matrix[abs+i][ord] != null && matrix[abs+i][ord].getIdPlayer() == lastToken.getIdPlayer()){
            alignedToken++;
            i++;
        }
        return alignedToken >= 4;
    }


}

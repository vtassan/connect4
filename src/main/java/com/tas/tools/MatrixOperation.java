package com.tas.tools;

public class MatrixOperation {


    //TODO integrate to the matrix object??

    /**
     * test if the column of the matrix at the given abscissa is full or not
     * @param matrix the given matrix
     * @param abscissa the given abscissa
     * @return
     */
    public static boolean isColumnFull(Object[][] matrix, int abscissa){
        for(int i=0; i<matrix[abscissa].length;i++){
            if (matrix[abscissa][i] == null)
                    return false;
        }
        return true;
    }

    /**
     * return the lowest free spot in the column of the matrix at the given abscissa
     * @param matrix the matrix where to find the space
     * @param abscissa the abscissa of the column in the matrix
     * @return ordinate of the first free spot
     */
    public static int getFirstFreeSpot(Object[][] matrix, int abscissa){
        //start with the maximum height of the column
        int i=matrix[abscissa].length-1;
        //browse the column until the end or until finding another token
        while(i >= 0 && matrix[abscissa][i] == null){
            i--;
        }
        return i+1;
    }

}

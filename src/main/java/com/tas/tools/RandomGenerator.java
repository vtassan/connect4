package com.tas.tools;

import com.tas.entity.Board;

import java.util.Random;

public final class RandomGenerator {

    public final static Random RandomGenerator = new Random();

    public static int getRandomColumn(){
        return RandomGenerator.nextInt(Board.BOARD_WIDTH);
    }

}

package com.tas.tools;

import java.util.logging.Logger;

public interface LoggerInterface {

    public default Logger getLogger(){
        return Logger.getLogger(this.getClass().toString());
    }

}

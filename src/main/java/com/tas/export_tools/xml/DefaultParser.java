package com.tas.export_tools.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DefaultParser {

    private File file;

    public DefaultParser(File file) {
        this.file = file;
    }

    public NodeList processText(String expression) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        FileInputStream fileIS = new FileInputStream(this.getFile());
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document xmlDocument = builder.parse(fileIS);
        XPath xPath = XPathFactory.newInstance().newXPath();

        NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
        return nodeList;
    }

    void printChild(Node node){
        System.out.println(node.getTextContent());
        if(node.hasChildNodes()){
            NodeList nl = node.getChildNodes();
            int len = nl.getLength();

            for (int i = 0; i < len; i++) {
                printChild(nl.item(i));
            }
        }
    }

    File getFile(){
        return this.file;
    }

    public static void main(String... args) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {
        DefaultParser df = new DefaultParser(new File("src/main/java/com/tas/export_tools/xml/tuto.xml"));
        NodeList nodeList = df.processText("/Tutorials/Tutorial");
        for (int i = 0; i < nodeList.getLength(); i++) {
            df.printChild(nodeList.item(i));
            System.out.println("iteration #"+i);
        }

    }
}
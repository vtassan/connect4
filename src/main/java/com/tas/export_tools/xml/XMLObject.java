package com.tas.export_tools.xml;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public interface XMLObject {


    static final Set<Class<?>> WRAPPER_TYPES = getWrapperTypes();

    public static Object[] getFieldValues(Field[] fields, Object instance) throws IllegalAccessException {
        Object[] values = new Object[fields.length];
        for (int i = 0; i <fields.length ; i++) {
            Field field = fields[i];
            System.out.println(field.getType()+" : "+field.getName());
            field.setAccessible(true);
            values[i] = field.get(instance);
        }
        return values;
    }

    public default String toXML() {
        return toXML(this,0);
    }

    private static String toXML(Object object, int indentLevel) {
        if(object == null){
            return "";
        }
        else if (object.getClass().isPrimitive()) {
            return object + "";
        }
        else if(isWrapperType(object.getClass())) {
            return object+ "";
        }
        else if(object.getClass().toString().equals("String")) {
            return object.toString();
        }
        else if(object.getClass().isArray()) { //TODO manage all kind of collections
            StringBuilder arrayStringBuilder = new StringBuilder();
            System.out.println(object.getClass());
            Object[] array = (Object[]) object;

            for (int i = 0; i < array.length; i++) {
                String xmlArrayElement = toXML(array[i], indentLevel+1);
//                arrayStringBuilder.append(appendIndented(xmlArrayElement,indentLevel));
                arrayStringBuilder.append(xmlArrayElement);
            }
            return new String(arrayStringBuilder);
        }
        else if(object instanceof XMLObject && !(object instanceof XMLConverter)){
            return  ((XMLObject) object).getXMLSignature();
        }
        else{
            StringBuilder xmlString = new StringBuilder();
            String[] completeName = splitModuleAndClassName(object.getClass());
            String folderName = completeName[0];
            String className = completeName[1];

            xmlString.append("<" + className + " module=\"" + folderName + "\"" + ">\n");
//            xmlString.append(appendIndented("<" + className + " module=\"" + folderName + "\"" + ">\n",indentLevel));
            Field[] fields = object.getClass().getDeclaredFields();
            Object[] values = null;
            try {
                values = getFieldValues(fields, object);
                for (int i = 0; i < values.length; i++) {
                    String attributeName = getFieldName(fields[i]);
                    String attributeClass = fields[i].getType().toString();
                    xmlString.append("<"+attributeName+" class=\""+attributeClass+"\">\n");
                    xmlString.append(toXML(values[i],indentLevel+1));
                    xmlString.append("\n");
                    xmlString.append("</"+attributeName+">\n");
//                    xmlString.append(appendIndented("<"+attributeName+" class=\""+attributeClass+"\">\n",indentLevel));
//                    xmlString.append(appendIndented(toXML(values[i],indentLevel+1),0));
//                    xmlString.append(appendIndented("</"+attributeName+">\n",indentLevel));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            xmlString.append("</" + className +">\n");
            String buff = appendIndented("</" + className +">\n",indentLevel);
//            xmlString.append(buff);
            return new String(xmlString);
        }
    }

    private static String[] splitModuleAndClassName(Class clazz){
        String completeClassName = clazz.getName();
        int indexClassName = completeClassName.lastIndexOf('.');
        String className = completeClassName.substring(indexClassName + 1);
        String folderName = completeClassName.substring(0, indexClassName);
        return new String[]{folderName,className};
    }

    private static String getFieldName(Field field){
        return field.getName();
    }

    private static String appendIndented(String string, int indentLevel){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < indentLevel; i++) {
            sb.append("\t");
            sb.append(string);
        }
        return new String(sb);
    }

    private static Set<Class<?>> getWrapperTypes(){
        Set<Class<?>> ret = new HashSet<>();
        ret.add(Boolean.class);
        ret.add(Character.class);
        ret.add(Byte.class);
        ret.add(Short.class);
        ret.add(Integer.class);
        ret.add(Long.class);
        ret.add(Float.class);
        ret.add(Double.class);
        ret.add(Void.class);
        return ret;
    }

    private static boolean isWrapperType(Class<?> clazz){
        return WRAPPER_TYPES.contains(clazz);
    }

    String getXMLSignature();

    /**
     * Generate the attributes of the object based on the xml given
     * @param string
     */
    public void toObject(String string);

}

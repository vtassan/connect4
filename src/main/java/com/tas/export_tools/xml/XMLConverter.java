package com.tas.export_tools.xml;

import java.io.FileWriter;
import java.io.IOException;

public interface XMLConverter extends XMLObject{


    default void writeXMLObject(String path) throws IOException {
        try(FileWriter fileWriter = new FileWriter(path)){
            fileWriter.write("<?xml version=\"1.0\"?>\n");
            String xml = this.toXML();
            fileWriter.write(xml);
            fileWriter.flush();
        }

    }

    @Override
    public default String getXMLSignature(){
        try {
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}

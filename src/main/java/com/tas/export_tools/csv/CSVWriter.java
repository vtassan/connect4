package com.tas.export_tools.csv;

import java.io.*;

public class CSVWriter<T> {

    private T[][] matrix;

    public CSVWriter ( T[][] matrix){
        this.matrix = matrix;
    }

    public void write(String filename) throws IOException {

        FileWriter fileWriter = new FileWriter(filename);

        try(BufferedWriter outputWriter = new BufferedWriter(fileWriter)){

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length-1; j++) {
                    outputWriter.write(matrix[i][j].toString());
                    outputWriter.write(CSVTools.COMMA_DELIMITER);
                }
                outputWriter.write(matrix[i][matrix[i].length-1].toString());
                outputWriter.write("\n");
            }
            outputWriter.flush();
        }
    }

}

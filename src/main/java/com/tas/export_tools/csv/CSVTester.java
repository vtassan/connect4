package com.tas.export_tools.csv;

import java.io.IOException;

public class CSVTester {

    public static void main(String... args) throws IOException {

        String file1 = "src/test/csv/testString.csv";
        String file2 = "src/test/csv/testInt.csv";

        String[][] testWriter = {{"John","Bill"},{"Boulanger","Mécanicien"}};
        CSVWriter<String> writer = new CSVWriter(testWriter);

        writer.write(file1);

        Integer[][] testInt = {{1,2},{3,4}};
        CSVWriter<Integer> writer2 = new CSVWriter(testInt);

        writer2.write(file2);

        CSVReader testReader = new CSVReader(file2);
        String[][] stringMatrix = testReader.readFile();
        int[][] matrix = testReader.convertToInt(stringMatrix);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }

    }
}

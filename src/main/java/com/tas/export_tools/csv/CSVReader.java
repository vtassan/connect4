package com.tas.export_tools.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {

    private String filename;

    public CSVReader(String filename){
        this.filename = filename;
    }

    public String[][] readFile() throws IOException {
        List<String[]> records = new ArrayList<>();
        int rowSize = -1;
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(CSVTools.COMMA_DELIMITER+"");
                records.add(values);
                if(rowSize == -1){
                    rowSize = values.length;
                }else{
                    assert rowSize == values.length;
                }
            }
        }
        String[][] result = new String[records.size()][rowSize];
        return  records.toArray(result);
    }

    public int[][] convertToInt(String[][] stringMatrix){
        int[][] intMatrix = new int[stringMatrix.length][stringMatrix[0].length];
        for (int i = 0; i < stringMatrix.length; i++) {
            for (int j = 0; j < stringMatrix[i].length; j++) {
                intMatrix[i][j] = Integer.parseInt(stringMatrix[i][j]);
            }
        }
        return intMatrix;
    }

    public double[][] convertToDouble(String[][] stringMatrix){
        double[][] doubleMatrix = new double[stringMatrix.length][stringMatrix[0].length];
        for (int i = 0; i < stringMatrix.length; i++) {
            for (int j = 0; j < stringMatrix[i].length; j++) {
                doubleMatrix[i][j] = Double.parseDouble(stringMatrix[i][j]);
            }
        }
        return doubleMatrix;
    }

}

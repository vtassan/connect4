package com.tas.entity;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class GameTest {

    @Test
    public void testActivePlayer(){
        int i = 0;
        for(int k = 0; k<100; k++){
            i = (i+1)%2;
            assertTrue(i==0 || i== 1);
        }
    }

}

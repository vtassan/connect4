package com.tas.entity;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BoardTest {

    @Test
    public void testConstructor(){
        Board board = new Board();

        assertEquals(7, board.getMatrix().length);
        assertEquals(6, board.getMatrix()[0].length);
    }
}

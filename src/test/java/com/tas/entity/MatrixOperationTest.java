package com.tas.entity;

import com.tas.tools.MatrixOperation;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class MatrixOperationTest {

    @Test
    public void testMatrixOperation(){
        Board board = new Board();

        for(int i=0;i<board.getMatrix().length;i++){
            boolean full = MatrixOperation.isColumnFull(board.getMatrix(),i);
            assertTrue(!full);
        }
        int res = MatrixOperation.getFirstFreeSpot(board.getMatrix(),3);
        assertEquals(0,res);

    }
}
